#include <iostream>
#include <array>


using namespace std;



int main() 
{
    // initialize the c style array
    int c_arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

    // get the size of the c array
    const int size = sizeof(c_arr) / sizeof(c_arr[0]);

    // declare the c++ array
    array<int, size> cpp_arr;

    int even_count = 0, odd_count = 0;



    // insert elements from the C-style array to the C++ array
    for (int i = 0; i < size; i++)
    {
        cpp_arr[i] = c_arr[i];
    }


    
    // count the amount of odd and even numbers
    for (int i = 0; i < size; i++)
    {
        if (cpp_arr[i] % 2 == 0)
        {
            even_count++;
        }
        else
        {
            odd_count++;
        }
    }



    // check if odd or even numbers appear more

    string comparison = "greater than";
    
    if (odd_count > even_count)
    {
        comparison = "less than";
    }
    else if (odd_count == even_count)
    {
        comparison = "equal to";
    }



    // print the output to the terminal

    cout << "\nHoang Phuc Luan Truong --- Assignment 1\n\n";

    cout << "The array has: " << size << " numbers\n";

    cout << "There are " << even_count << " even numbers"
         << " and " << odd_count << " odd numbers\n\n";

    cout << "The amount of even numbers is " << comparison
         << " the amount of odd numbers\n\n";

    
    return 0;
}