#include <iostream>
#include <vector>
#include <string>
#include <array>
#include "vowel_stats.h"
 
using namespace std;



int main() 
{
    string str;
    string* arr;


    // prompt for input string
    cout << endl << "Please enter a string:" << endl;
    getline(cin, str);


    int len = str.length();


    // create the array and vector based on the input string

    arr = new string[len];
    vector<string> vect(len);

    for (int i = 0; i < len; i++)
    {
        arr[i] = str[i];
        vect[i] = str[i];
    }


    // get statistics of the array and print it out on terminal
    cout << endl << "RUNNING TEST ON ARRAY" << endl;
    get_array_stats(arr, len);
    display_stats();


    // get statistics of the vector and print it out on terminal
    cout << endl << "RUNNING TEST ON VECTOR" << endl;
    get_vector_stats(vect);
    display_stats();
    

    // clean heap
    delete[] arr;
}