#include <iostream>
#include <vector>
#include <chrono>
#include "vowel_stats.h"

using namespace std;


int a_num, e_num, i_num, o_num, u_num;
double time_executed;



void check_vowel(string);




/**
 * Check for number of vowels in a vector of string and 
 * see how long it takes to finish the check
 * 
 * @param vect the vector to be checked
 */
void get_vector_stats(vector<string> vect)
{
    // reset the vowels count
    a_num = 0, e_num = 0, i_num = 0, o_num = 0, u_num = 0;

    // start timing
    chrono::time_point<chrono::system_clock> start_time = chrono::system_clock::now();

    for (int i = 0; i < vect.size(); i++)
    {
        check_vowel(vect[i]);
    }

    // end timing
    chrono::time_point<chrono::system_clock> end_time = chrono::system_clock::now();

    // save the time in nanoseconds
    time_executed = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();
}





/**
 * Check for number of vowels in an array of string and
 * see how long it takes to finish the check
 * 
 * @param arr the array to be checked
 * @param arr_len the length of the array
 */
void get_array_stats(string arr[], int arr_len)
{   
    // reset the vowels count
    a_num = 0, e_num = 0, i_num = 0, o_num = 0, u_num = 0;

    // start timing
    chrono::time_point<chrono::system_clock> start_time = chrono::system_clock::now();

    for (int i = 0; i < arr_len; i++)
    {
        check_vowel(arr[i]);
    }

    // end timing
    chrono::time_point<chrono::system_clock> end_time = chrono::system_clock::now();

    // save the time in nanoseconds
    time_executed = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();
}





/**
 * Display the statistics collected on the console
 */
void display_stats()
{
    cout << endl << "-----------------------------------" << endl;
    cout << "a: " << a_num << endl;
    cout << "e: " << e_num << endl;
    cout << "i: " << i_num << endl;
    cout << "o: " << o_num << endl;
    cout << "u: " << u_num << endl;

    cout << "Elapsed time: " << time_executed << " ns";
    cout << endl << endl;
}





/**
 * Check if a letter is a vowel. If it is, then increase
 * the count for the corresponding vowel.
 * 
 * @param c the letter to be checked
 */
void check_vowel(string c)
{
    switch (tolower(c[0]))
    {
        case 'a':
            a_num++;
            break;
        
        case 'e':
            e_num++;
            break;

        case 'i':
            i_num++;
            break;

        case 'o':
            o_num++;
            break;

        case 'u':
            u_num++;
            break;
            
        default:
            break;
    }
}