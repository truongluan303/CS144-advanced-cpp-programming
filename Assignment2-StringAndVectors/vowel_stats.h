#include <vector>

using namespace std;


void get_vector_stats(vector<string> vect);

void get_array_stats(string arr[], int arr_len);

void display_stats();