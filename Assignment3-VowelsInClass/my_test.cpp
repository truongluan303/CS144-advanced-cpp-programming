#include <iostream>
#include <vector>
#include <string>
#include <sstream>

#include "vowel_stats.h"


using namespace std;



////// Function Prototypes //////

void print_word_art();

vector<string> convert_to_vector(string input);




/* -----------------------------
 * - - - - - M A I N - - - - - -
 * -----------------------------
 */
int main()
{
    string input;
    VowelStats vowel_stats;


    print_word_art();


    // prompt the user to input a string
    cout << "Please enter a string: ";
    getline(cin, input);


    // convert the string to a vector
    vector<string> vec = convert_to_vector(input);


    // run the test
    vowel_stats.run_tests(vec);


    // display the output
    vowel_stats.display_stats();

    cout << endl << endl << "Press Enter to continue...";
    cin.ignore();


    return 0;
}






/**
 * Split the words in a string and put them in a vector
 * @return a vector of string based on the given input string
 */
vector<string> convert_to_vector(string input)
{
    vector<string> vec;
    stringstream ssin(input);
    unsigned int i = 0;
    string temp;

    while (ssin >> temp)
    {
        vec.push_back(temp);
    }
    return vec;
}






/**
 * Print out the headline for the program
 */
void print_word_art()
{
    string word_art = R"(
,--.   ,--.                       ,--.            ,---.   ,--.            ,--.
 \  `.'  /,---. ,--.   ,--. ,---. |  | ,---.     '   .-',-'  '-. ,--,--.,-'  '-. ,---.
  \     /| .-. ||  |.'.|  || .-. :|  |(  .-'     `.  `-.'-.  .-'' ,-.  |'-.  .-'(  .-'
   \   / ' '-' '|   .'.   |\   --.|  |.-'  `)    .-'    | |  |  \ '-'  |  |  |  .-'  `)
    `-'   `---' '--'   '--' `----'`--'`----'     `-----'  `--'   `--`--'  `--'  `----')";

    cout << word_art << endl << endl;
}







