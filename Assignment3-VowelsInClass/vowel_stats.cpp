#include <chrono>
#include <iostream>
#include "vowel_stats.h"


using namespace std;



/**
 * Create an array based on the given vector.
 * Then run test on both the vector and array.
 * @param test_list the vector to run test on
 */
void VowelStats::run_tests(vector<string> test_list)
{
    // reset all the vowels count and vowel tracker
    for (auto item : VOWEL_IDX)
    {
        vector_vowels_count[item.second] = 0;
        array_vowels_count[item.second] = 0;
    }



    empty_input = test_list.empty();
    if (empty_input)
    {
        // if the given vector is empty, use the default one
        test_list = DEFAULT_TEST_LIST;
    }

    
    unsigned int size = test_list.size();


    // create an array based on the given vector
    string* arr = new string[size];

    for (int i = 0; i < size; i++)
    {
        arr[i] = test_list[i];
    }


    // get the stats for the array and the vector

    get_array_stats(arr, size);

    get_vector_stats(test_list);


    // map each count to its correspoding vowel 
    // so we don't loose track of each vowel's location after sorting
    map_count_to_vowel();


    // run sorting algorithms on the count for array

    int* temp = array_vowels_count;

    quick_sort(false);

    for (unsigned short i = 0; i < NUM_OF_VOWELS; i++)
        array_vowels_count[i] = temp[i];                // reset arr to do another sort

    bubble_sort(false);


    // run sorting algorithms on the count for vector

    temp = vector_vowels_count;    

    quick_sort(true);

    for (unsigned short i = 0; i < NUM_OF_VOWELS; i++)
        vector_vowels_count[i] = temp[i];                // reset arr to do another sort

    bubble_sort(true);


    // clean the heap

    delete[] arr;
}






/**
 * Display the collected information on the screen, including
 * vowels count, elapsed time for counting and sorting.
 */
void VowelStats::display_stats()
{
    if (empty_input)
    {
        cout << "Input was empty. A default string was used instead!" << endl << endl << endl;
    }
    cout << endl << endl;


    // Result for vector
    cout << "Vector vowels count:";

    print_vowels(vector_vowels_count);
    cout << endl;

    cout << endl << "\tCounting Elapsed Time: " << vector_total_time << " " << TIME_UNIT << endl;
    
    cout << endl << "\tBubble Sort Elapsed Time: " << bubble_sort_vector_time << " " << TIME_UNIT << endl;

    cout << endl << "\tQuick Sort Elapsed Time: " << quick_sort_vector_time << " " << TIME_UNIT << endl << endl;


    // Result for array
    cout << "Array vowels count:";

    print_vowels(array_vowels_count);
    cout << endl;
    
    cout << endl << "\tCounting Elapsed time: " << array_total_time << " " << TIME_UNIT << endl;

    cout << endl << "\tBubble Sort Elapsed Time: " << bubble_sort_array_time << " " << TIME_UNIT << endl;

    cout << endl << "\tQuick Sort Elapsed Time: " << quick_sort_array_time << " " << TIME_UNIT << endl << endl;

    
    cout << endl << endl;
    
    cout << "counting + bubble sort (vector): " <<  bubble_sort_vector_time +  vector_total_time << " " << TIME_UNIT << endl << endl;

    cout << "counting + bubble sort (array): " <<  bubble_sort_array_time + array_total_time << " " << TIME_UNIT << endl << endl;

    cout << "counting + quicksort (vector): " <<  quick_sort_vector_time + vector_total_time << " " << TIME_UNIT << endl << endl;

    cout << "counting + quicksort (array): " <<  quick_sort_array_time + array_total_time << " " << TIME_UNIT << endl << endl;
}





/**
 * Count the number of vowels in the vector and record the time consumed
 * @param vect the vector in which vowels will be counted
 */
void VowelStats::get_vector_stats(vector<string> vect)
{
    // start timing
    chrono::time_point<chrono::system_clock> start_time = chrono::system_clock::now();

    for (int i = 0; i < vect.size(); i++)
    {
        for (int j = 0; j < vect[i].length(); j++) 
        {
            char ch = (char)tolower(vect[i][j]);        // turn to lower case to avoid case-sensitive

            auto item = VOWEL_IDX.find(ch);         
            if (item != VOWEL_IDX.end())                // if the character is in the vowel map, then it is a vowel
            {
                vector_vowels_count[item->second]++;    // increase the counting value at the corresponding index
            }
        }
    }
    // end timing
    chrono::time_point<chrono::system_clock> end_time = chrono::system_clock::now();
    // calculate the total time consumed
    vector_total_time = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();
}






/**
 * Count the number of vowels in the array and record the time consumed
 * @param arr the array in which vowels will be counted
 * @param arr_length number of elements in the given array
 */
void VowelStats::get_array_stats(string arr[], int arr_length)
{
    // start timing
    chrono::time_point<chrono::system_clock> start_time = chrono::system_clock::now();

    for (int i = 0; i < arr_length; i++)
    {
        for (int j = 0; j < arr[i].length(); j++)
        {
            char ch = (char)tolower(arr[i][j]);     // turn to lower case to avoid case-sensitive

            auto item = VOWEL_IDX.find(ch);
            if (item != VOWEL_IDX.end())            // if the character is in the vowel map, then it is a vowel
            {
                array_vowels_count[item->second]++; // increase the counting value at the corresponding index
            }
        }
    }
    // end timing
    chrono::time_point<chrono::system_clock> end_time = chrono::system_clock::now();
    // calculate the total time consumed
    array_total_time = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();
}






/**
 * Call bubble sort on vector vowels count if the given boolean is true.
 * Else, call bubble sort on array vowels count
 * @param bool determine whether or not to call on vector
 */
void VowelStats::bubble_sort(bool is_for_vector)
{
    // begin timing
    chrono::time_point<chrono::system_clock> start_time = chrono::system_clock::now();

    if (is_for_vector)
    {
        bubble_sort(vector_vowels_count, NUM_OF_VOWELS);
    }
    else
    {
        bubble_sort(array_vowels_count, NUM_OF_VOWELS);
    }

    // end time
    chrono::time_point<chrono::system_clock> end_time = chrono::system_clock::now();
    double total_time = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();

    if (is_for_vector)
    {
        bubble_sort_vector_time = total_time;
    }
    else
    {
        bubble_sort_array_time = total_time;
    }
}





/**
 * Perform bubble sort on the given array
 * @param arr array to be sorted
 * @param size size of the given array
 */
void VowelStats::bubble_sort(int arr[], int size)
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size - i - 1; j++) 
        {
            if (arr[j] > arr[j + 1])
            {
                swap(arr[j], arr[j + 1]);
            }
        }
    }
}






/**
 * Call quick sort on vector vowels count if the given boolean is true.
 * Else, call quick  sort on array vowels count
 * @param bool determine whether or not to call on vector
 */
void VowelStats::quick_sort(bool is_for_vector)
{
    // start timing
    chrono::time_point<chrono::system_clock> start_time = chrono::system_clock::now();

    if (is_for_vector)
    {
        quick_sort(vector_vowels_count, 0, NUM_OF_VOWELS);
    }
    else
    {
        quick_sort(array_vowels_count, 0, NUM_OF_VOWELS);
    }

    // end time
    chrono::time_point<chrono::system_clock>end_time = chrono::system_clock::now();
    double total_time = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time).count();

    if (is_for_vector)
    {
        quick_sort_vector_time = total_time;
    }
    else
    {
        quick_sort_array_time = total_time;
    }
}






/**
 * Perform quicksort on the given array
 * @param arr array to be sorted
 * @param begin beginning index
 * @param end ending index
 */
void VowelStats::quick_sort(int arr[], int begin, int end)
{
    if (begin < end)
    {
        int pivot_idx = end - 1;
        int k = begin - 1;

        for (int i = begin; i < end; i++)
        {
            if (arr[i] < arr[pivot_idx])
            {
                k++;
                // swap arr[k] and arr[i]
                swap(arr[k], arr[i]);
            }
        }

        // put the pivot to its correct position
        swap(arr[pivot_idx], arr[k + 1]);

        pivot_idx = k + 1;

        quick_sort(arr, begin, pivot_idx);
        quick_sort(arr, pivot_idx + 1, end);
    }
}





/**
 * Swap 2 elements in an array
 * @param a first element
 * @param b second element
 */
void VowelStats::swap(int& a, int& b)
{
    int temp = a;

    a = b;

    b = temp;
}






/**
 * Print the count of the vowels on the screen
 */
void VowelStats::print_vowels(int count[])
{
    unordered_set<char> printed;
    
    for (unsigned short i = 0; i < NUM_OF_VOWELS; i++)
    {
        int count = vector_vowels_count[i];

        // get the vowel(s) which has that count
        unordered_set<char> vowels = num_to_vowel[count];

        for (char ch : vowels)
        {
            if (printed.find(ch) == printed.end())
            {
                cout << "\t'" << ch << "' -> " << count;
                printed.insert(ch);
            }
        }
    }
}







/**
 * map the number of count to the corresponding vowel so that
 * we don't loose track of the location of each vowel in the array
 * after the array is sorted
 */
void VowelStats::map_count_to_vowel()
{
    num_to_vowel.clear();

    for (auto item : VOWEL_IDX)
    {
        if (num_to_vowel.find(array_vowels_count[item.second]) == num_to_vowel.end())
        {
            unordered_set<char> temp = {item.first};
            num_to_vowel[array_vowels_count[item.second]] = temp;
        }
        // if the key (count) exists, 
        // it means 2 or more vowels are having the same occurence
        // therefore, add that to the vector
        else
        {
            num_to_vowel[array_vowels_count[item.second]].insert(item.first);
        }
    }
}