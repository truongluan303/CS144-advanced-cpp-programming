#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>

#define NUM_OF_VOWELS 5

using namespace std;


class VowelStats
{
private:

    ////// constant values //////

    // map the vowel to its index in the arrays that store the vowels count
    const unordered_map<char, int> VOWEL_IDX = {{'a', 0}, {'e', 1}, {'i', 2}, {'o', 3}, {'u', 4}};

    // default test vector in case the input vector is empty
    const vector<string> DEFAULT_TEST_LIST = {"USA", "UK", "Japan", "China", "Germany", "Italy", 
        "Vietnam", "Taiwan", "France", "Mexico", "Brazil", "Egypt", "Thailand", "Korea", "Singapore",
        "Philippines", "Russia", "Argentina", "Ethiopia", "Australia"
    };

    const string TIME_UNIT = "nanoseconds";


    ////// private variables //////

    double array_total_time;                    // the time it takes to count vowels in array

    double vector_total_time;                   // the time it takes to count vowels in vector

    int array_vowels_count[NUM_OF_VOWELS];      // store the count for array

    int vector_vowels_count[NUM_OF_VOWELS];     // store the count for vector

    double bubble_sort_array_time;              // the time it takes to finish bubble sort on array

    double bubble_sort_vector_time;             // the time it takes to finish bubble sort on vector

    double quick_sort_array_time;               // the time it takes to finish quick sort on array

    double quick_sort_vector_time;              // the time it takes to finish quick sort on vector

    bool empty_input;                           // true if the input vector in empty

    unordered_map<int, unordered_set<char>> num_to_vowel; // map the count to its vowel -> track vowel location after sort


    ////// private functions //////

    void get_vector_stats(vector<string> vect);         // run test on string vector

    void get_array_stats(string arr[], int arr_length); // run test on string array

    void check_vowel(char ch);                          // check for vowel and increase the coresponding count

    void bubble_sort(bool is_for_vector);               // calling function for bubble sort

    void quick_sort(bool is_for_vector);                // calling function for quick sort

    void bubble_sort(int arr[], int size);              // perform bubble sort

    void quick_sort(int arr[], int begin, int end);     // perform quick sort

    void swap(int& a, int& b);                          // swap 2 elemets

    void map_count_to_vowel();                          // map the count to its corresponding vowel

    void print_vowels(int count[]);                     // print the vowel and its count



public:


    ////// public functions //////

    void display_stats();

    void run_tests(vector<string> test_list);
};