#include <iostream>
#include <vector>
#include <sstream>
#include <iomanip>

#include "io_runner.h"

using namespace std;


// define the files' names as constants
const string INPUT_FILE = "ramen-ratings.csv";
const string OUTPUT_FILE = "output.csv";


// function prototypes
void press_to_continue();
void display_noodle_reviews(vector<NoodleReview>);





////////////////////////////////////////////////////////////////////////////
//                              M A I N                                   //
////////////////////////////////////////////////////////////////////////////
int main()
{
    IoRunner io_runner;
    vector<NoodleReview> reviews_list;

    reviews_list = io_runner.read_from_file(INPUT_FILE);

    int return_result = io_runner.write_to_file(OUTPUT_FILE, reviews_list);

    if (return_result != 0)
    {
        cout << "There has been a problem writing to " << OUTPUT_FILE << endl;
    }

    display_noodle_reviews(reviews_list);

    press_to_continue();
    return 0;
}





/**
 * @brief print all the reviews to the terminal
 * 
 * @param vec_nr the vector containing all the reviews
 */
void display_noodle_reviews(vector<NoodleReview> vec_nr)
{
    for (unsigned int i = 0; i < vec_nr.size(); i++)
    {
        cout << string_representation(vec_nr[i]) << endl;
    }
    cout << endl;
}





/**
 * @param noodle_review the NoodleReview object to be represented as string
 * 
 * @return a string representation of the NoodleReview object
 * 
 */
string string_representation(NoodleReview noodle_review)
{
    ostringstream ss;
    ss << fixed << setprecision(1) << noodle_review.stars;

    string stars = ss.str();
    string top_ten = (noodle_review.top_ten != "") ? (", " + noodle_review.top_ten) : "";

    return (
        to_string(noodle_review.review) + ", " +
        noodle_review.brand + ", " +
        noodle_review.variety + ", " +
        noodle_review.style + ", " +
        noodle_review.country + ", " +
        stars + top_ten  
    );
}





/**
 * @brief prompt the user to press enter in order to continue
 */
void press_to_continue()
{
    cout << endl << endl << "Press Enter to continue...";
    cin.ignore();
    cout << endl << endl;
}