#include <iostream>
#include <fstream>
#include <sstream>

#include "io_runner.h"

#define NUM_OF_COLS 7


using namespace std;



// prototypes for helper functions

vector<string> tokenize(string, string);    // tokenize a string

bool is_numeric(string);                    // check is a string is numeric





/**
 * @brief Default Constructor
 * 
 */
IoRunner::IoRunner()
{
    cout << "FileIO class object has started" << endl << endl;
}





/**
 * @brief Read a file a extract each line's information to create a NoodleReview object.
 * 
 * @param file_name the name of the file to be read
 * 
 * @return a vector containing all of the NoodleReview objects created
 * 
 */
vector<NoodleReview> IoRunner::read_from_file(string file_name)
{
    vector<NoodleReview> result;
    ifstream in_file(file_name);
    string line;

    if (!in_file.is_open() || !in_file.good())
    {   
        // throw exception if there's a problem when opening the file
        throw runtime_error("An error occured when opening \"" + file_name + "\"");
    }

    // the first line will contain the names of the columns
    getline(in_file, headline);

    // read every line from the file and create NoodleReview object
    while (getline(in_file, line))
    {
        vector<string> tokens = tokenize(line, ",");

        if (tokens.size() != NUM_OF_COLS)
        {
            // skip those that don't have enough tokens
            continue;
        }

        int review_num = (is_numeric(tokens[0])) ? stoi(tokens[0]) : -1;
        string brand = tokens[1];
        string variety = tokens[2];
        string style = tokens[3];
        string country = tokens[4];
        double stars = (is_numeric(tokens[5])) ? stod(tokens[5]) : -1;
        string top = tokens[6];

        // add the newly created NoodleReview instance to the result vector
        NoodleReview review(review_num, brand, variety, style, country, stars, top);
        result.push_back(review);
    }

    // close the file object and return
    in_file.close();
    return result;
}





/**
 * @brief write the reviews to a file where each field is separated by a comma
 * 
 * @param file_name the name of the output file
 * @param input_data the vector containing all the reviews to be written
 * 
 * @return 0 if the writing process was successful, 1 otherwise
 * 
 */
int IoRunner::write_to_file(string file_name, vector<NoodleReview> input_data)
{
    ofstream out_file;
    out_file.open(file_name);

    if (out_file.fail())
    {
        // return 1 if there is problem when opening the file
        return 1;
    }

    out_file << headline << endl;
    
    for (unsigned int i = 0; i < input_data.size(); i++)
    {
        int review = input_data[i].get_review_number();
        string brand = input_data[i].get_brand();
        string variety = input_data[i].get_variety();
        string style = input_data[i].get_style();
        string country = input_data[i].get_country();
        double stars = input_data[i].get_stars();
        string top_ten = input_data[i].get_top_ten();

        // send data to the stream
        out_file << "\"" << review << "\",";
        out_file << "\"" << brand << "\",";
        out_file << "\"" << variety << "\",";
        out_file << "\"" << style << "\",";
        out_file << "\"" << country << "\",";
        out_file << "\"" << stars << "\",";
        out_file << "\"" << top_ten << "\"\n";
    }

    // close the output stream and return 0
    out_file.close();
    return 0;
}





/**
 * Split a string into a vector where each word is an item in the vector
 * 
 * @param s the string to be splitted
 * @param sep indicates when to seperate the string
 * 
 * @return a vector containing all the words splitted from the given string
 * 
 */
vector<string> tokenize(string s, string sep)
{
    vector<string> result;
    int start = 0;
    int end = s.find(sep);

    while (end != -1) 
    {
        result.push_back(s.substr(start, end - start));
        start = end + sep.size();
        end = s.find(sep, start);
    }
    string last_token = s.substr(start, end - start);
    
    if (last_token.find("\"") == string::npos)
    {
        result.push_back(last_token);
    }
    
    return result;
}





/**
 * Check if all characters in the given string are numeric
 * 
 * @param s the string to be checked
 * 
 * @return true if the whole string is numeric 
 * 
 */
bool is_numeric(string s)
{
    for (unsigned int i = 0; i < s.length(); i++)
    {
        if (!isdigit(s[i]) && s[i] != '.' && s[i] != ',')
        {
            return false;
        }
    }
    return true;
}