#ifndef FILE_IO_H
#define FILE_IO_H

#include "file_io.h"



/**
 * IoRunner class
 * 
 * @brief Inherits from the class FileIO. This class deals with file input and output.
 * 
 */
class IoRunner : public FileIO
{
private:

    string headline;

public:

    IoRunner();

    vector<NoodleReview> read_from_file(string) override;

    int write_to_file(string, vector<NoodleReview>) override;
};


#endif