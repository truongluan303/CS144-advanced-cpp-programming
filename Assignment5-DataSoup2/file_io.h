#define FILE_IO_H

#include <vector>

#include "noodle_review.h"

using namespace std;



class FileIO
{
    virtual vector<NoodleReview> read_from_file(string file_name) = 0;

    virtual int write_to_file(string file_name, vector<NoodleReview> input_data) = 0;
};
