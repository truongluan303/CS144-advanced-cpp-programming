#include "noodle_review.h"

using namespace std;



NoodleReview::NoodleReview(int review, string brand, string variety, string style,
                           string country, double stars, string top_ten)
{
    this->review = review;
    this->brand = brand;
    this->variety = variety;
    this->style = style;
    this->country = country;
    this->stars = stars;
    this->top_ten = top_ten;
}


int NoodleReview::get_review_number() { return review; }


string NoodleReview::get_brand() { return brand; }


string NoodleReview::get_variety() { return variety; }


string NoodleReview::get_style() { return style; }


string NoodleReview::get_country() { return country; }


double NoodleReview::get_stars() { return stars; }


string NoodleReview::get_top_ten() { return top_ten; }