#ifndef NOODLE_REVIEW_H
#define NOODLE_REVIEW_H

#include <string>

using namespace std;



/**
 * NoodleReview class
 * 
 * @brief represents a review for a noodle/ramen product
 * 
 */
class NoodleReview
{
private:

    int review;

    string brand;

    string variety;

    string style;

    string country;

    double stars;

    string top_ten;


public:

    NoodleReview(int, string, string, string, string, double, string);

    int get_review_number();

    string get_brand();

    string get_variety();

    string get_style();

    string get_country();

    double get_stars();

    string get_top_ten();

    friend string string_representation(NoodleReview noodle_review);
};


#endif