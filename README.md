# CS144 - Advanced C++ Programming ![icons8-c++](https://user-images.githubusercontent.com/83048295/161918178-94d4baf1-fee3-47d5-b7e6-b6467d5e0166.svg) 
Containing the projects done for the C++ programming class at San Jose State University.

### Concepts Covered
The projects covered basic/intermediate concepts in C++ programming such as file reading/writing, pointers, memory management, GUI programming, etc.
There are also a few projects that cover data structure and algorithms concept (sorting, linked-list, etc)
